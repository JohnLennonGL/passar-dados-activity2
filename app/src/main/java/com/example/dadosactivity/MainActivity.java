package com.example.dadosactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {


    private Button botao;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botao = findViewById(R.id.buttonDados);

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = (new Intent(MainActivity.this , DadosActivity.class));
                intent.putExtra("nome","John");

                // instanciando e chamando objetos
                Usuario usuario = new Usuario("John","john.lennon.lobo@gmail.com");
                intent.putExtra("usuario",usuario);


                // startar activiy
                startActivity(intent);
            }
        });
    }
}
