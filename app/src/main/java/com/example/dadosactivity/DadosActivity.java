package com.example.dadosactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class DadosActivity extends AppCompatActivity {



    private TextView texto, texto2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados);

        texto = findViewById(R.id.textopadrao);
        texto2 = findViewById(R.id.texto2ID);
        // recuperar dados enviados
        Bundle extras = getIntent().getExtras();
        String nome = extras.getString("nome");

        // passando objetos
        Usuario usuario = (Usuario) extras.get("usuario");
        texto.setText(usuario.getEmail());
        texto2.setText(usuario.getNome());
        }



}
